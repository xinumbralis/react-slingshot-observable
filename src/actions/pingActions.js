// example of a thunk using the redux-thunk middleware
export function ping() {
  return function (dispatch) {
    // thunks allow for pre-processing actions, calling apis, and dispatching multiple actions
    // in this case at this point we could call a service that would persist the fuel savings
    console.log('dispatch ping');
    return dispatch({
      type: 'PING',
    });
  };
}

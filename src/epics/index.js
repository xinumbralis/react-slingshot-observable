import { combineEpics } from 'redux-observable';

const pingEpic = action$ =>
  action$.ofType('PING').do(x => console.log('middleware ping'))
    .delay(1000) // Asynchronously wait 1000ms then continue
    .mapTo({ type: 'PONG' });

const rootEpic = pingEpic;

export default rootEpic;

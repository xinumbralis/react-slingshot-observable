import objectAssign from 'object-assign';
import initialState from './initialState';

export default function pingReducer(state = initialState.isPinging, action) {
  switch (action.type) {
    case 'PING':
      console.log('reduce ping');
      return true;

    case 'PONG':
      console.log('reduce pong');
      return false;

    default:
      return state;
  }
}

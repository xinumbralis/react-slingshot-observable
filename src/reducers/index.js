import { combineReducers } from 'redux';
import fuelSavings from './fuelSavingsReducer';
import isPinging from './pingReducer';
import {routerReducer} from 'react-router-redux';

const rootReducer = combineReducers({
  fuelSavings,
  isPinging,
  routing: routerReducer
});

export default rootReducer;
